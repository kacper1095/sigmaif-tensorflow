import unittest

import numpy as np
import tensorflow as tf

from sigmaif.group_classes_initializers import *
from sigmaif.layer import SigmaIf, sigmaif


def test_sigmaif_layer_init():
    layer = SigmaIf(512, 0.8, tf.nn.relu)
    assert layer.units == 512
    assert layer.threshold == 0.8


def test_sigmaif_layer_build():
    x = tf.placeholder(tf.float32, shape=(2, 6))
    sort = tf.placeholder(tf.bool)
    output = sigmaif([x, sort], 3, 0.8)
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        assert output[0].get_shape().as_list() == [2, 3]
        assert output[1].get_shape().as_list() == [2, 3]


def test_sigmaif_layer_feedforward():
    x = tf.placeholder(tf.float32, shape=(2, 6))
    sort = tf.placeholder(tf.bool)
    output = sigmaif([x, sort], 3, 0.8)
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        samples = np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]])
        result, used_neurons = sess.run(output, feed_dict={
            x: samples,
            sort: True
        })
        assert result.shape == (2, 3)
        assert used_neurons.shape == (2, 3)
    assert output[0].shape.as_list() == [2, 3]


def test_sigmaif_variable_elements_shapes():
    x = tf.placeholder(tf.float32, shape=(None, 6))
    sort = tf.placeholder(tf.bool)
    layer = SigmaIf(2, 0.8, group_initializer=Regular(3))
    _ = layer([x, sort])
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        weights = layer.kernel
        groups = layer.groups
        group_classes = layer.group_classes

        np_weights, np_groups, np_group_classes = sess.run([weights, groups, group_classes])
    assert np_weights.shape == (6, 2)
    assert np_groups.shape == (2, 6)
    assert np_group_classes.shape == (6,)
    np.testing.assert_array_equal(np_group_classes, np.asarray([0, 0, 0, 1, 1, 1], dtype=np.int32))


def test_sigmaif_layer_feedforward_huge_data_block():
    x = tf.placeholder(tf.float32, shape=(None, 1024))
    sort = tf.placeholder(tf.bool)
    output = sigmaif([x, sort], 10, 0.8)
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        samples = np.random.normal(0, scale=1, size=(2, 1024))
        result, used_neurons = sess.run(output, feed_dict={
            x: samples,
            sort: True
        })
        assert result.shape == (2, 10)
        assert used_neurons.shape == (2, 10)
    assert output[0].shape.as_list() == [None, 10]


def test_sigmaif_layer_sorting():
    layer = SigmaIf(3, 0.8)
    x = tf.placeholder(tf.float32, shape=(2, 6))
    sort = tf.placeholder(tf.bool)
    output = layer([x, sort])

    # necessary to perform keras.Layer build() method
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        samples = np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]])
        result, used_neurons = sess.run(output, feed_dict={
            x: samples,
            sort: False
        })

        assert result.shape == (2, 3)
        assert used_neurons.shape == (2, 3)

    dummy_weights = np.asarray([[0.64477137, -0.28826308, -0.52849554],
                                [-0.01434887, 0.03962899, -0.77525493],
                                [0.09964093, 0.41641266, 0.45743252],
                                [0.58601077, -0.15051486, 0.36372773],
                                [-0.45316896, -0.97923837, 0.07542192],
                                [0.66336406, 0.52651525, -0.5792045]], dtype=np.float32)
    indices_before = layer.groups
    assign_weights = tf.assign(layer.weights[0], dummy_weights)
    indices_after = layer.groups

    expected_indices = np.asarray([
        [5, 0, 3, 2, 1, 4],
        [5, 2, 1, 3, 0, 4],
        [2, 3, 4, 0, 5, 1]
    ], dtype=np.int32)
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        sess.run(assign_weights)
        indices_before = sess.run(indices_before)
        samples = np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]])
        _ = sess.run(output, feed_dict={
            x: samples,
            sort: True
        })
        indices_after = sess.run(indices_after)
        weights_after = sess.run(layer.weights[0])

    assert indices_after.shape == indices_before.shape
    np.testing.assert_array_equal(weights_after, dummy_weights)
    np.testing.assert_array_equal(expected_indices, indices_after)
    np.testing.assert_raises(AssertionError, np.testing.assert_array_equal, indices_after, indices_before)


def test_sigmaif_layer_train_on_single_point():
    np.random.seed(0xCAFFE)
    tf.set_random_seed(0xCAFFE)

    x = tf.placeholder(tf.float32, shape=(None, 6))
    y = tf.placeholder(tf.float32, shape=(None, 3))
    sort = tf.placeholder(tf.bool)
    output, used_neurons = sigmaif([x, sort], 3, 2, tf.nn.sigmoid)
    loss = tf.reduce_mean(tf.reduce_sum(tf.pow(output - y, 2), reduction_indices=[1]))

    accuracy = tf.reduce_mean(tf.cast(tf.equal(
        tf.argmax(y, axis=1), tf.argmax(output, axis=1)
    ), dtype=tf.float32))

    train_op = tf.train.AdamOptimizer(0.1).minimize(loss)

    sample = np.random.normal(0, 1, size=(3, 6)).astype(np.float32)
    classes = np.asarray([
        [1, 0, 0],
        [0, 1, 0],
        [0, 0, 1]
    ], dtype=np.float32)
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        best_accuracy = 0
        best_loss = 10
        current_loss = 0

        for i in range(100):
            print(f"Epoch {i + 1}/{100}")
            _, l, a, = sess.run([train_op, loss, accuracy], feed_dict={
                x: sample,
                y: classes,
                sort: i > 0 and i % 20 == 0

            })
            if i == 0:
                current_loss = l
            best_accuracy = max(a, best_accuracy)
            best_loss = min(l, best_loss)
            print(f"Loss: {best_loss:.4f}, Acc: {best_accuracy:.4f}")

    np.testing.assert_almost_equal(best_accuracy, 1)
    assert best_loss < 0.1
    assert best_loss < current_loss


def test_exponential_grouping_method():
    exp = exponential(2)
    sequence = exp.generate_length_sequence(10)
    sequence = np.asarray(sequence)
    assert sequence.shape == (10,)
    np.testing.assert_array_equal(sequence, [0, 1, 1, 2, 2, 2, 2, 3, 3, 3])


def test_fibonacci_grouping_method():
    fib = fibonacci()
    sequence = fib.generate_length_sequence(10)
    sequence = np.asarray(sequence)
    assert sequence.shape == (10,)
    np.testing.assert_array_equal(sequence, [0, 1, 2, 2, 3, 3, 3, 4, 4, 4])


def test_regular_grouping_method():
    reg = regular(3)
    output = reg((10, 4))
    expected_result = np.asarray([0, 0, 0, 1, 1, 1, 2, 2, 2, 3], dtype=np.float32)
    with tf.Session() as sess:
        result = sess.run(output)
        assert result.shape == (10,)
        np.testing.assert_array_equal(result, expected_result)


if __name__ == '__main__':
    unittest.main()
