import numpy as np
import tensorflow as tf

from sigmaif import sigma_if_lib

NUMERICAL_CHECK_EPSILON = 1e-4


def test_sigmaif_all_as_group_with_sorting():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        sort = tf.placeholder(tf.bool)
        W = tf.constant(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        fading = 1.
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32)
        group_classes = tf.constant([0, 0, 0, 0, 0, 0], tf.int32)

        sess.run(tf.global_variables_initializer())
        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)

        o = sess.run(sigmaif, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                         sort: True})
        result, used_neurons, sorted_array = o
        np.testing.assert_array_equal(sorted_array, np.asarray([[5, 4, 3, 2, 1, 0]]))
        np.testing.assert_array_equal(result, [[21.], [42.]])
        np.testing.assert_array_equal(used_neurons, [[6], [6]])
        np.testing.assert_array_equal(used_neurons.shape, (2, 1))

        assign_op = groups.assign(sorted_array)
        eval_groups = sess.run(assign_op)

        np.testing.assert_array_equal(eval_groups, np.asarray([[5, 4, 3, 2, 1, 0]]))


def test_sigmaif_all_as_group_without_sorting():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        sort = tf.placeholder(tf.bool)
        W = tf.constant(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        fading = 1
        groups = tf.constant(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32)
        group_classes = tf.constant([0, 0, 0, 0, 0, 0], tf.int32)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)

        o = sess.run(sigmaif, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                         sort: False})

        result, used_neurons, sorted_array = o
        np.testing.assert_array_equal(sorted_array, np.asarray([[1, 0, 2, 4, 3, 5]]))
        np.testing.assert_array_equal(result, [[21.], [42.]])
        np.testing.assert_array_equal(used_neurons, [[6], [6]])
        np.testing.assert_array_equal(used_neurons.shape, (2, 1))


def test_sigmaif_single_elem_per_group():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        fading = 1
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 2, 3, 4, 5], tf.int32)
        sort = tf.placeholder(tf.bool)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)

        sess.run(tf.global_variables_initializer())
        o = sess.run(sigmaif, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                         sort: False})
        result, used_groups, sorted_indices = o

        groups.assign(sorted_indices)
        np.testing.assert_array_equal(result, [[11.], [12.]])
        np.testing.assert_array_equal(used_groups, [[4], [3]])
        np.testing.assert_array_equal(used_groups.shape, (2, 1))


def test_sigmaif_single_elem_per_group_with_sorting_enabled():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(3, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        fading = 1
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 2, 3, 4, 5], tf.int32)
        sort = tf.placeholder(tf.bool)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)

        sess.run(tf.global_variables_initializer())
        o = sess.run(sigmaif, feed_dict={x: np.asarray([
            [0, 1, 1.5, 2, 2.5, 3], [1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]
        ], dtype=np.float32), sort: True})
        result, used_groups, sorted_indices = o

        groups.assign(sorted_indices)
        np.testing.assert_array_equal(result, [[18], [11.], [12.]])
        np.testing.assert_array_equal(used_groups, [[1], [2], [1]])
        np.testing.assert_array_equal(used_groups.shape, (3, 1))


def test_sigmaif_growing_groups():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        # [2, 1, 3, 5, 4, 6]

        threshold = tf.constant(5, dtype=tf.float32)
        fading = 1
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 1, 2, 2, 2], tf.int32)
        sort = tf.placeholder(tf.bool)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)

        sess.run(tf.global_variables_initializer())
        o = sess.run(sigmaif, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                         sort: False})
        result, used_groups, sorted_indices = o

        groups.assign(sorted_indices)
        np.testing.assert_array_equal(result, [[6.], [12.]])
        np.testing.assert_array_equal(used_groups, [[3], [3]])
        np.testing.assert_array_equal(used_groups.shape, (2, 1))


def test_sigmaif_weights_gradients():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 2, 3, 4, 5], tf.int32)
        sort = tf.placeholder(tf.bool)
        fading = 1

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)
        res = sigmaif.result
        gradients = tf.gradients(res, W)
        sess.run(tf.global_variables_initializer())
        gradients_np = \
            sess.run(gradients, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                           sort: False})[0]
        expected_result = np.asarray([[3], [3], [3], [0.], [1], [0.]])
        assert expected_result.shape == gradients_np.shape
        np.testing.assert_array_equal(expected_result, gradients_np)


def test_sigmaif_weights_gradients_numerical_check():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 2, 3, 4, 5], tf.int32)
        sort = tf.placeholder(tf.bool)
        fading = 1

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)
        res = sigmaif.result
        gradients = tf.gradients(res, W)

        sess.run(tf.global_variables_initializer())
        gradients_np = \
            sess.run(gradients, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                           sort: False})[0]
        expected_result = np.asarray([[3], [3], [3], [0.], [1], [0.]])
        assert expected_result.shape == gradients_np.shape
        np.testing.assert_array_equal(expected_result, gradients_np)

        for i in range(6):
            mask_vector = np.zeros((6, 1))
            mask_vector[i, 0] = NUMERICAL_CHECK_EPSILON
            numerical_res_forward = sigma_if_lib.sigma_if(x, W + mask_vector, threshold,
                                                          fading, fading, groups,
                                                          group_classes, sort).result
            numerical_res_backward = sigma_if_lib.sigma_if(x, W - mask_vector, threshold,
                                                           fading, fading,
                                                           groups, group_classes, sort).result
            numerical_gradients = (numerical_res_forward - numerical_res_backward) / (2 * NUMERICAL_CHECK_EPSILON)
            numerical_gradients_np = \
                sess.run(numerical_gradients,
                         feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                    sort: False})

            np.testing.assert_almost_equal(np.sum(numerical_gradients_np), gradients_np[i, 0], decimal=2)


def test_sigmaif_weights_two_outputs_gradients_numerical_check():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6], [2, 3, 1, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5], [0, 1, 4, 3, 2, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 2, 3, 4, 5], tf.int32)
        sort = tf.placeholder(tf.bool)
        fading = 1

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)
        res = sigmaif.result
        gradients = tf.gradients(res, W)

        sess.run(tf.global_variables_initializer())
        gradients_np = \
            sess.run(gradients, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                           sort: False})[0]

        for i in range(6):
            for j in range(2):
                mask = np.zeros((6, 2))
                mask[i, j] = NUMERICAL_CHECK_EPSILON
                numerical_res_forward = sigma_if_lib.sigma_if(x, W + mask, threshold,
                                                              fading, fading, groups,
                                                              group_classes, sort).result
                numerical_res_backward = sigma_if_lib.sigma_if(x, W - mask, threshold,
                                                               fading, fading,
                                                               groups, group_classes, sort).result
                numerical_gradients = (numerical_res_forward - numerical_res_backward) / (2 * NUMERICAL_CHECK_EPSILON)
                numerical_gradients_np = \
                    sess.run(numerical_gradients,
                             feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                        sort: False})

                np.testing.assert_almost_equal(np.sum(numerical_gradients_np[:, j]), gradients_np[i, j], decimal=2)


def test_sigmaif_inputs_gradients():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        fading = 1
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 2, 3, 4, 5], tf.int32)
        sort = tf.placeholder(tf.bool)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)
        res = sigmaif.result
        gradients = tf.gradients(res, x)
        sess.run(tf.global_variables_initializer())
        gradients_np = \
            sess.run(gradients, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                           sort: False})[0]
        expected_result = np.asarray([
            [1, 2, 3, 0, 5, 0],
            [1, 2, 3, 0, 0, 0]
        ])
        assert expected_result.shape == gradients_np.shape
        np.testing.assert_array_equal(gradients_np, expected_result)


def test_sigmaif_inputs_gradients_numerical_check():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        fading = 1
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 2, 3, 4, 5], tf.int32)
        sort = tf.placeholder(tf.bool)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)
        res = sigmaif.result
        gradients = tf.gradients(res, x)
        sess.run(tf.global_variables_initializer())
        gradients_np = \
            sess.run(gradients, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                           sort: False})[0]
        expected_result = np.asarray([
            [1, 2, 3, 0, 5, 0],
            [1, 2, 3, 0, 0, 0]
        ])
        assert expected_result.shape == gradients_np.shape
        np.testing.assert_array_equal(gradients_np, expected_result)

        for i in range(2):
            for j in range(6):
                mask = np.zeros((2, 6), dtype=np.float32)
                mask[i, j] = NUMERICAL_CHECK_EPSILON
                numerical_res_forward = sigma_if_lib.sigma_if(x + mask, W, threshold,
                                                              fading, fading, groups,
                                                              group_classes, sort).result
                numerical_res_backward = sigma_if_lib.sigma_if(x - mask, W, threshold,
                                                               fading, fading,
                                                               groups, group_classes, sort).result
                numerical_gradients = (numerical_res_forward - numerical_res_backward) / (2 * NUMERICAL_CHECK_EPSILON)
                numerical_gradients_np = \
                    sess.run(numerical_gradients,
                             feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                        sort: False})

                np.testing.assert_almost_equal(np.sum(numerical_gradients_np[i, :]), gradients_np[i, j], decimal=2)


def test_sigmaif_inputs_two_outputs_gradients_numerical_check():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6], [2, 3, 1, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5], [0, 1, 4, 3, 2, 5]]), dtype=tf.int32, trainable=False)
        fading = 1
        group_classes = tf.constant([0, 1, 2, 3, 4, 5], tf.int32)
        sort = tf.placeholder(tf.bool)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)
        res = sigmaif.result
        gradients = tf.gradients(res, x)
        sess.run(tf.global_variables_initializer())
        gradients_np = \
            sess.run(gradients, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                           sort: False})[0]
        for i in range(2):
            for j in range(6):
                mask = np.zeros((2, 6))
                mask[i, j] = NUMERICAL_CHECK_EPSILON
                numerical_res_forward = sigma_if_lib.sigma_if(x + mask, W, threshold,
                                                              fading, fading, groups,
                                                              group_classes, sort).result
                numerical_res_backward = sigma_if_lib.sigma_if(x - mask, W, threshold,
                                                               fading, fading,
                                                               groups, group_classes, sort).result
                numerical_gradients = (numerical_res_forward - numerical_res_backward) / (2 * NUMERICAL_CHECK_EPSILON)
                numerical_gradients_np = \
                    sess.run(numerical_gradients,
                             feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                        sort: False})

                np.testing.assert_almost_equal(np.sum(numerical_gradients_np[i, :]), gradients_np[i, j], decimal=2)


def test_sigmaif_enable_sorting():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        fading = 1
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 2, 3, 4, 5], tf.int32)
        sort = tf.placeholder(tf.bool)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)
        sorted_group_indices = sigmaif.sorted_group_indices
        assign_op = tf.assign(groups, sorted_group_indices)
        with tf.control_dependencies([assign_op]):
            result_op = sigmaif.result + 0

        sess.run(tf.global_variables_initializer())
        _ = sess.run(result_op, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                           sort: True})[0]

        sorted_groups = sess.run(groups)
        np.testing.assert_raises(AssertionError, np.testing.assert_array_equal,
                                 np.asarray([[1, 0, 2, 4, 3, 5]], dtype=np.int32), sorted_groups)
        np.testing.assert_array_equal(sorted_groups, np.asarray([[5, 4, 3, 2, 1, 0]], dtype=np.int32))


def test_sigmaif_two_output_neuron_sorting():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6], [4, 2, -1, 3, -100, 10]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        fading = 1
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5], [0, 5, 4, 1, 2, 3]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 2, 3, 4, 5], tf.int32)
        sort = tf.placeholder(tf.bool)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)
        sorted_group_indices = sigmaif.sorted_group_indices
        assign_op = tf.assign(groups, sorted_group_indices)
        with tf.control_dependencies([assign_op]):
            result_op = sigmaif.result + 0

        sess.run(tf.global_variables_initializer())
        _ = sess.run(result_op, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                           sort: True})[0]

        sorted_groups = sess.run(groups)
        np.testing.assert_raises(AssertionError, np.testing.assert_array_equal,
                                 np.asarray([[1, 0, 2, 4, 3, 5], [0, 5, 4, 1, 2, 3]], dtype=np.int32), sorted_groups)
        np.testing.assert_array_equal(sorted_groups,
                                      np.asarray([[5, 4, 3, 2, 1, 0], [5, 0, 3, 1, 2, 4]], dtype=np.int32))


def test_sigmaif_disable_sorting():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        fading = 1
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 2, 3, 4, 5], tf.int32)
        sort = tf.placeholder(tf.bool)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)
        sorted_group_indices = sigmaif.sorted_group_indices
        assign_op = tf.assign(groups, sorted_group_indices)
        with tf.control_dependencies([assign_op]):
            result_op = sigmaif.result + 0

        sess.run(tf.global_variables_initializer())
        _, sorted_group_indices_np = sess.run([result_op, sorted_group_indices], feed_dict={
            x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
            sort: False})

        unsorted_groups = sess.run(groups)
        np.testing.assert_array_equal(np.asarray([[1, 0, 2, 4, 3, 5]], dtype=np.int32), unsorted_groups)
        np.testing.assert_raises(AssertionError, np.testing.assert_array_equal, unsorted_groups,
                                 np.asarray([[5, 4, 3, 2, 1, 0]], dtype=np.int32))


def test_sigmaif_disable_sorting_2_outputs():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6], [1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        fading = 1
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5], [1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 2, 3, 4, 5], tf.int32)
        sort = tf.placeholder(tf.bool)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)
        sorted_group_indices = sigmaif.sorted_group_indices
        assign_op = tf.assign(groups, sorted_group_indices)
        with tf.control_dependencies([assign_op]):
            result_op = sigmaif.result + 0

        sess.run(tf.global_variables_initializer())
        _, sorted_group_indices_np = sess.run([result_op, sorted_group_indices], feed_dict={
            x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
            sort: False})

        unsorted_groups = sess.run(groups)
        np.testing.assert_array_equal(np.asarray([[1, 0, 2, 4, 3, 5], [1, 0, 2, 4, 3, 5]], dtype=np.int32),
                                      unsorted_groups)
        np.testing.assert_raises(AssertionError, np.testing.assert_array_equal, unsorted_groups,
                                 np.asarray([[5, 4, 3, 2, 1, 0], [5, 4, 3, 2, 1, 0]], dtype=np.int32))


def test_ffa_sigmaif_all_as_group_with_sorting():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        sort = tf.placeholder(tf.bool)
        W = tf.constant(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        fading = 0.7
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32)
        group_classes = tf.constant([0, 0, 0, 0, 0, 0], tf.int32)

        sess.run(tf.global_variables_initializer())
        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)

        o = sess.run(sigmaif, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                         sort: True})
        result, used_neurons, sorted_array = o
        np.testing.assert_array_equal(sorted_array, np.asarray([[5, 4, 3, 2, 1, 0]]))
        np.testing.assert_almost_equal(result, np.asarray([[21], [42]], dtype=np.float32))
        np.testing.assert_array_equal(used_neurons, [[6], [6]])
        np.testing.assert_array_equal(used_neurons.shape, (2, 1))

        assign_op = groups.assign(sorted_array)
        eval_groups = sess.run(assign_op)

        np.testing.assert_array_equal(eval_groups, np.asarray([[5, 4, 3, 2, 1, 0]]))


def test_ffa_sigmaif_all_as_group_without_sorting():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        sort = tf.placeholder(tf.bool)
        W = tf.constant(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        fading = 0.7
        groups = tf.constant(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32)
        group_classes = tf.constant([0, 0, 0, 0, 0, 0], tf.int32)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)

        o = sess.run(sigmaif, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                         sort: False})

        result, used_neurons, sorted_array = o
        np.testing.assert_array_equal(sorted_array, np.asarray([[1, 0, 2, 4, 3, 5]]))
        np.testing.assert_almost_equal(result, np.asarray([[21], [42]], dtype=np.float32))
        np.testing.assert_array_equal(used_neurons, [[6], [6]])
        np.testing.assert_array_equal(used_neurons.shape, (2, 1))


def test_ffa_sigmaif_single_elem_per_group():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        fading = 0.7
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 2, 3, 4, 5], tf.int32)
        sort = tf.placeholder(tf.bool)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)

        sess.run(tf.global_variables_initializer())
        o = sess.run(sigmaif, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                         sort: False})
        result, used_groups, sorted_indices = o

        groups.assign(sorted_indices)
        np.testing.assert_almost_equal(result, [[9.7932], [9.36]], decimal=5)
        np.testing.assert_array_equal(used_groups, [[5], [3]])
        np.testing.assert_array_equal(used_groups.shape, (2, 1))


def test_ffa_sigmaif_single_elem_per_group_with_sorting_enabled():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(3, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        fading = 0.7
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 2, 3, 4, 5], tf.int32)
        sort = tf.placeholder(tf.bool)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)

        sess.run(tf.global_variables_initializer())
        o = sess.run(sigmaif, feed_dict={x: np.asarray([
            [0, 1, 1.5, 2, 2.5, 3], [1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]
        ], dtype=np.float32), sort: True})
        result, used_groups, sorted_indices = o

        groups.assign(sorted_indices)
        np.testing.assert_allclose(result, [[18], [9.2], [12.]])
        np.testing.assert_allclose(used_groups, [[1], [2], [1]])
        np.testing.assert_allclose(used_groups.shape, (3, 1))


def test_ffa_sigmaif_growing_groups():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)

        threshold = tf.constant(5, dtype=tf.float32)
        fading = 0.7
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 1, 2, 2, 2], tf.int32)
        sort = tf.placeholder(tf.bool)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)

        sess.run(tf.global_variables_initializer())
        o = sess.run(sigmaif, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                         sort: False})
        result, used_groups, sorted_indices = o

        groups.assign(sorted_indices)
        np.testing.assert_allclose(result, [[5.4], [10.8]])
        np.testing.assert_allclose(used_groups, [[3], [3]])
        np.testing.assert_allclose(used_groups.shape, (2, 1))


def test_ffa_sigmaif_weights_gradients():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 2, 3, 4, 5], tf.int32)
        sort = tf.placeholder(tf.bool)
        fading = 0.7

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)
        res = sigmaif.result
        gradients = tf.gradients(res, W)
        sess.run(tf.global_variables_initializer())
        gradients_np = \
            sess.run(gradients, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                           sort: False})[0]
        expected_result = np.asarray([[1.743], [1.2201], [2.49], [1.], [0.7], [0.]])
        assert expected_result.shape == gradients_np.shape
        np.testing.assert_allclose(expected_result, gradients_np)


def test_ffa_sigmaif_weights_gradients_numerical_check():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 2, 3, 4, 5], tf.int32)
        sort = tf.placeholder(tf.bool)
        fading = 0.7

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)
        res = sigmaif.result
        gradients = tf.gradients(res, W)
        sess.run(tf.global_variables_initializer())
        gradients_np = \
            sess.run(gradients, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                           sort: False})[0]
        expected_result = np.asarray([[1.743], [1.2201], [2.49], [1.], [0.7], [0.]])
        assert expected_result.shape == gradients_np.shape
        np.testing.assert_allclose(expected_result, gradients_np)

        for i in range(6):
            mask_vector = np.zeros((6, 1))
            mask_vector[i, 0] = NUMERICAL_CHECK_EPSILON
            numerical_res_forward = sigma_if_lib.sigma_if(x, W + mask_vector, threshold,
                                                          fading, fading, groups,
                                                          group_classes, sort).result
            numerical_res_backward = sigma_if_lib.sigma_if(x, W - mask_vector, threshold,
                                                           fading, fading,
                                                           groups, group_classes, sort).result
            numerical_gradients = (numerical_res_forward - numerical_res_backward) / (2 * NUMERICAL_CHECK_EPSILON)
            numerical_gradients_np = \
                sess.run(numerical_gradients,
                         feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                    sort: False})

            np.testing.assert_almost_equal(np.sum(numerical_gradients_np), gradients_np[i, 0], decimal=2)


def test_ffa_sigmaif_weights_gradients_with_growing_groups():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 1, 2, 2, 3], tf.int32)
        sort = tf.placeholder(tf.bool)
        fading = 0.7

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)
        res = sigmaif.result
        gradients = tf.gradients(res, W)
        sess.run(tf.global_variables_initializer())
        gradients_np = \
            sess.run(gradients, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                           sort: False})[0]
        expected_result = np.asarray([[2.7], [1.89], [2.7], [1], [1], [0.]])
        assert expected_result.shape == gradients_np.shape
        np.testing.assert_allclose(expected_result, gradients_np)


def test_ffa_sigmaif_inputs_gradients():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        fading = 0.7
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 2, 3, 4, 5], tf.int32)
        sort = tf.placeholder(tf.bool)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)
        res = sigmaif.result
        gradients = tf.gradients(res, x)
        sess.run(tf.global_variables_initializer())
        gradients_np = \
            sess.run(gradients, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                           sort: False})[0]
        expected_result = np.asarray([
            [0.343, 0.4802, 1.47, 4, 3.5, 0],
            [0.7, 0.98, 3, 0, 0, 0]
        ])
        assert expected_result.shape == gradients_np.shape
        np.testing.assert_allclose(gradients_np, expected_result)


def test_ffa_sigmaif_inputs_gradients_numerical_check():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        fading = 0.7
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 2, 3, 4, 5], tf.int32)
        sort = tf.placeholder(tf.bool)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)
        res = sigmaif.result
        gradients = tf.gradients(res, x)
        sess.run(tf.global_variables_initializer())
        gradients_np = \
            sess.run(gradients, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                           sort: False})[0]
        expected_result = np.asarray([
            [0.343, 0.4802, 1.47, 4, 3.5, 0],
            [0.7, 0.98, 3, 0, 0, 0]
        ])
        assert expected_result.shape == gradients_np.shape
        np.testing.assert_allclose(gradients_np, expected_result)

        for i in range(2):
            for j in range(6):
                mask = np.zeros((2, 6))
                mask[i, j] = NUMERICAL_CHECK_EPSILON
                numerical_res_forward = sigma_if_lib.sigma_if(x + mask, W, threshold,
                                                              fading, fading, groups,
                                                              group_classes, sort).result
                numerical_res_backward = sigma_if_lib.sigma_if(x - mask, W, threshold,
                                                               fading, fading,
                                                               groups, group_classes, sort).result
                numerical_gradients = (numerical_res_forward - numerical_res_backward) / (2 * NUMERICAL_CHECK_EPSILON)
                numerical_gradients_np = \
                    sess.run(numerical_gradients,
                             feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                        sort: False})

                np.testing.assert_almost_equal(np.sum(numerical_gradients_np), gradients_np[i, j], decimal=2)


def test_ffa_sigmaif_inputs_gradients_with_growing_groups():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        fading = 0.7
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 1, 2, 2, 3], tf.int32)
        sort = tf.placeholder(tf.bool)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)
        res = sigmaif.result
        gradients = tf.gradients(res, x)
        sess.run(tf.global_variables_initializer())
        gradients_np = \
            sess.run(gradients, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                           sort: False})[0]
        expected_result = np.asarray([
            [0.7, 0.98, 2.1, 4.0, 5.0, 0],
            [1, 1.4, 3, 0, 0, 0]
        ])
        assert expected_result.shape == gradients_np.shape
        np.testing.assert_allclose(gradients_np, expected_result)


def test_ffa_sigmaif_enable_sorting():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        fading = 0.7
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 2, 3, 4, 5], tf.int32)
        sort = tf.placeholder(tf.bool)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)
        sorted_group_indices = sigmaif.sorted_group_indices
        assign_op = tf.assign(groups, sorted_group_indices)
        with tf.control_dependencies([assign_op]):
            result_op = sigmaif.result + 0

        sess.run(tf.global_variables_initializer())
        _ = sess.run(result_op, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                           sort: True})[0]

        sorted_groups = sess.run(groups)
        np.testing.assert_raises(AssertionError, np.testing.assert_array_equal,
                                 np.asarray([[1, 0, 2, 4, 3, 5]], dtype=np.int32), sorted_groups)
        np.testing.assert_array_equal(sorted_groups, np.asarray([[5, 4, 3, 2, 1, 0]], dtype=np.int32))


def test_ffa_sigmaif_disable_sorting():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        fading = 0.7
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 2, 3, 4, 5], tf.int32)
        sort = tf.placeholder(tf.bool)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)
        sorted_group_indices = sigmaif.sorted_group_indices
        assign_op = tf.assign(groups, sorted_group_indices)
        with tf.control_dependencies([assign_op]):
            result_op = sigmaif.result + 0

        sess.run(tf.global_variables_initializer())
        _, sorted_group_indices_np = sess.run([result_op, sorted_group_indices], feed_dict={
            x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
            sort: False})

        unsorted_groups = sess.run(groups)
        np.testing.assert_array_equal(np.asarray([[1, 0, 2, 4, 3, 5]], dtype=np.int32), unsorted_groups)
        np.testing.assert_raises(AssertionError, np.testing.assert_array_equal, unsorted_groups,
                                 np.asarray([[5, 4, 3, 2, 1, 0]], dtype=np.int32))


def test_ffa_sigmaif_disable_sorting_2_outputs():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6], [1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        fading = 0.7
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5], [1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 2, 3, 4, 5], tf.int32)
        sort = tf.placeholder(tf.bool)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, fading, fading, groups, group_classes, sort)
        sorted_group_indices = sigmaif.sorted_group_indices
        assign_op = tf.assign(groups, sorted_group_indices)
        with tf.control_dependencies([assign_op]):
            result_op = sigmaif.result + 0

        sess.run(tf.global_variables_initializer())
        _, sorted_group_indices_np = sess.run([result_op, sorted_group_indices], feed_dict={
            x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
            sort: False})

        unsorted_groups = sess.run(groups)
        np.testing.assert_array_equal(np.asarray([[1, 0, 2, 4, 3, 5], [1, 0, 2, 4, 3, 5]], dtype=np.int32),
                                      unsorted_groups)
        np.testing.assert_raises(AssertionError, np.testing.assert_array_equal, unsorted_groups,
                                 np.asarray([[5, 4, 3, 2, 1, 0], [5, 4, 3, 2, 1, 0]], dtype=np.int32))


def test_ffa_sigmaif_all_as_group_with_sorting_standard_backprop():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        sort = tf.placeholder(tf.bool)
        W = tf.constant(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        forward_fading = 0.7
        backward_fading = 1.0
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32)
        group_classes = tf.constant([0, 0, 0, 0, 0, 0], tf.int32)

        sess.run(tf.global_variables_initializer())
        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, forward_fading, backward_fading, groups, group_classes, sort)

        o = sess.run(sigmaif, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                         sort: True})
        result, used_neurons, sorted_array = o
        np.testing.assert_array_equal(sorted_array, np.asarray([[5, 4, 3, 2, 1, 0]]))
        np.testing.assert_almost_equal(result, np.asarray([[21], [42]], dtype=np.float32))
        np.testing.assert_array_equal(used_neurons, [[6], [6]])
        np.testing.assert_array_equal(used_neurons.shape, (2, 1))

        assign_op = groups.assign(sorted_array)
        eval_groups = sess.run(assign_op)

        np.testing.assert_array_equal(eval_groups, np.asarray([[5, 4, 3, 2, 1, 0]]))


def test_ffa_sigmaif_all_as_group_without_sorting_standard_backprop():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        sort = tf.placeholder(tf.bool)
        W = tf.constant(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        forward_fading = 0.7
        backward_fading = 1.0
        groups = tf.constant(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32)
        group_classes = tf.constant([0, 0, 0, 0, 0, 0], tf.int32)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, forward_fading, backward_fading, groups, group_classes, sort)

        o = sess.run(sigmaif, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                         sort: False})

        result, used_neurons, sorted_array = o
        np.testing.assert_array_equal(sorted_array, np.asarray([[1, 0, 2, 4, 3, 5]]))
        np.testing.assert_almost_equal(result, np.asarray([[21], [42]], dtype=np.float32))
        np.testing.assert_array_equal(used_neurons, [[6], [6]])
        np.testing.assert_array_equal(used_neurons.shape, (2, 1))


def test_ffa_sigmaif_single_elem_per_group_standard_backprop():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        forward_fading = 0.7
        backward_fading = 1.0
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 2, 3, 4, 5], tf.int32)
        sort = tf.placeholder(tf.bool)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, forward_fading, backward_fading, groups, group_classes, sort)

        sess.run(tf.global_variables_initializer())
        o = sess.run(sigmaif, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                         sort: False})
        result, used_groups, sorted_indices = o

        groups.assign(sorted_indices)
        np.testing.assert_almost_equal(result, [[9.7932], [9.36]], decimal=5)
        np.testing.assert_array_equal(used_groups, [[5], [3]])
        np.testing.assert_array_equal(used_groups.shape, (2, 1))


def test_ffa_sigmaif_single_elem_per_group_with_sorting_enabled_standard_backprop():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(3, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        forward_fading = 0.7
        backward_fading = 1.0
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 2, 3, 4, 5], tf.int32)
        sort = tf.placeholder(tf.bool)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, forward_fading, backward_fading, groups, group_classes, sort)

        sess.run(tf.global_variables_initializer())
        o = sess.run(sigmaif, feed_dict={x: np.asarray([
            [0, 1, 1.5, 2, 2.5, 3], [1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]
        ], dtype=np.float32), sort: True})
        result, used_groups, sorted_indices = o

        groups.assign(sorted_indices)
        np.testing.assert_allclose(result, [[18], [9.2], [12.]])
        np.testing.assert_allclose(used_groups, [[1], [2], [1]])
        np.testing.assert_allclose(used_groups.shape, (3, 1))


def test_ffa_sigmaif_growing_groups_standard_backprop():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)

        threshold = tf.constant(5, dtype=tf.float32)
        forward_fading = 0.7
        backward_fading = 1.0
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 1, 2, 2, 2], tf.int32)
        sort = tf.placeholder(tf.bool)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, forward_fading, backward_fading, groups, group_classes, sort)

        sess.run(tf.global_variables_initializer())
        o = sess.run(sigmaif, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                         sort: False})
        result, used_groups, sorted_indices = o

        groups.assign(sorted_indices)
        np.testing.assert_allclose(result, [[5.4], [10.8]])
        np.testing.assert_allclose(used_groups, [[3], [3]])
        np.testing.assert_allclose(used_groups.shape, (2, 1))


def test_ffa_sigmaif_weights_gradients_standard_backprop():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 2, 3, 4, 5], tf.int32)
        sort = tf.placeholder(tf.bool)
        forward_fading = 0.7
        backward_fading = 1.0

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, forward_fading, backward_fading, groups, group_classes, sort)
        res = sigmaif.result
        gradients = tf.gradients(res, W)
        sess.run(tf.global_variables_initializer())
        gradients_np = \
            sess.run(gradients, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                           sort: False})[0]
        expected_result = np.asarray([[3.], [3.], [3.], [1.], [1.], [0.]])
        assert expected_result.shape == gradients_np.shape
        np.testing.assert_allclose(expected_result, gradients_np)


def test_ffa_sigmaif_weights_gradients_with_growing_groups_standard_backprop():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 1, 2, 2, 3], tf.int32)
        sort = tf.placeholder(tf.bool)
        forward_fading = 0.7
        backward_fading = 1.0

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, forward_fading, backward_fading, groups, group_classes, sort)
        res = sigmaif.result
        gradients = tf.gradients(res, W)
        sess.run(tf.global_variables_initializer())
        gradients_np = \
            sess.run(gradients, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                           sort: False})[0]
        expected_result = np.asarray([[3.], [3.], [3.], [1.], [1.], [0.]])
        assert expected_result.shape == gradients_np.shape
        np.testing.assert_allclose(expected_result, gradients_np)


def test_ffa_sigmaif_inputs_gradients_standard_backprop():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        forward_fading = 0.7
        backward_fading = 1.0
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 2, 3, 4, 5], tf.int32)
        sort = tf.placeholder(tf.bool)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, forward_fading, backward_fading, groups, group_classes, sort)
        res = sigmaif.result
        gradients = tf.gradients(res, x)
        sess.run(tf.global_variables_initializer())
        gradients_np = \
            sess.run(gradients, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                           sort: False})[0]
        expected_result = np.asarray([
            [1, 2, 3, 4, 5, 0],
            [1, 2, 3, 0, 0, 0]
        ])
        assert expected_result.shape == gradients_np.shape
        np.testing.assert_allclose(gradients_np, expected_result)


def test_ffa_sigmaif_inputs_gradients_with_growing_groups_standard_backprop():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        forward_fading = 0.7
        backward_fading = 1.0
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 1, 2, 2, 3], tf.int32)
        sort = tf.placeholder(tf.bool)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, forward_fading, backward_fading, groups, group_classes, sort)
        res = sigmaif.result
        gradients = tf.gradients(res, x)
        sess.run(tf.global_variables_initializer())
        gradients_np = \
            sess.run(gradients, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                           sort: False})[0]
        expected_result = np.asarray([
            [1, 2, 3, 4, 5, 0],
            [1, 2, 3, 0, 0, 0]
        ])
        assert expected_result.shape == gradients_np.shape
        np.testing.assert_allclose(gradients_np, expected_result)


def test_ffa_sigmaif_enable_sorting_standard_backprop():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        forward_fading = 0.7
        backward_fading = 1.0
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 2, 3, 4, 5], tf.int32)
        sort = tf.placeholder(tf.bool)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, forward_fading, backward_fading, groups, group_classes, sort)
        sorted_group_indices = sigmaif.sorted_group_indices
        assign_op = tf.assign(groups, sorted_group_indices)
        with tf.control_dependencies([assign_op]):
            result_op = sigmaif.result + 0

        sess.run(tf.global_variables_initializer())
        _ = sess.run(result_op, feed_dict={x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
                                           sort: True})[0]

        sorted_groups = sess.run(groups)
        np.testing.assert_raises(AssertionError, np.testing.assert_array_equal,
                                 np.asarray([[1, 0, 2, 4, 3, 5]], dtype=np.int32), sorted_groups)
        np.testing.assert_array_equal(sorted_groups, np.asarray([[5, 4, 3, 2, 1, 0]], dtype=np.int32))


def test_ffa_sigmaif_disable_sorting_standard_backprop():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        forward_fading = 0.7
        backward_fading = 1.0
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 2, 3, 4, 5], tf.int32)
        sort = tf.placeholder(tf.bool)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, forward_fading, backward_fading, groups, group_classes, sort)
        sorted_group_indices = sigmaif.sorted_group_indices
        assign_op = tf.assign(groups, sorted_group_indices)
        with tf.control_dependencies([assign_op]):
            result_op = sigmaif.result + 0

        sess.run(tf.global_variables_initializer())
        _, sorted_group_indices_np = sess.run([result_op, sorted_group_indices], feed_dict={
            x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
            sort: False})

        unsorted_groups = sess.run(groups)
        np.testing.assert_array_equal(np.asarray([[1, 0, 2, 4, 3, 5]], dtype=np.int32), unsorted_groups)
        np.testing.assert_raises(AssertionError, np.testing.assert_array_equal, unsorted_groups,
                                 np.asarray([[5, 4, 3, 2, 1, 0]], dtype=np.int32))


def test_ffa_sigmaif_disable_sorting_2_outputs_standard_backprop():
    with tf.Session() as sess:
        x = tf.placeholder(tf.float32, shape=(2, 6))
        W = tf.Variable(np.asarray([[1, 2, 3, 4, 5, 6], [1, 2, 3, 4, 5, 6]]).T, dtype=tf.float32)
        threshold = tf.constant(9, dtype=tf.float32)
        forward_fading = 0.7
        backward_fading = 1.0
        groups = tf.Variable(np.asarray([[1, 0, 2, 4, 3, 5], [1, 0, 2, 4, 3, 5]]), dtype=tf.int32, trainable=False)
        group_classes = tf.constant([0, 1, 2, 3, 4, 5], tf.int32)
        sort = tf.placeholder(tf.bool)

        sigmaif = sigma_if_lib.sigma_if(x, W, threshold, forward_fading, backward_fading, groups, group_classes, sort)
        sorted_group_indices = sigmaif.sorted_group_indices
        assign_op = tf.assign(groups, sorted_group_indices)
        with tf.control_dependencies([assign_op]):
            result_op = sigmaif.result + 0

        sess.run(tf.global_variables_initializer())
        _, sorted_group_indices_np = sess.run([result_op, sorted_group_indices], feed_dict={
            x: np.asarray([[1, 1, 1, 1, 1, 1], [2, 2, 2, 2, 2, 2]], dtype=np.float32),
            sort: False})

        unsorted_groups = sess.run(groups)
        np.testing.assert_array_equal(np.asarray([[1, 0, 2, 4, 3, 5], [1, 0, 2, 4, 3, 5]], dtype=np.int32),
                                      unsorted_groups)
        np.testing.assert_raises(AssertionError, np.testing.assert_array_equal, unsorted_groups,
                                 np.asarray([[5, 4, 3, 2, 1, 0], [5, 4, 3, 2, 1, 0]], dtype=np.int32))
