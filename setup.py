from setuptools import find_packages, setup

setup(
    name="sigmaif",
    version="0.0.1",
    author="Kacper Kania",
    license="MIT",
    packages=find_packages(),
    author_email="kacper1095@gmail.com",
    zip_safe=False,
    tests_require=["pytest"],
    package_dir={"sigmaif": "sigmaif"},
    package_data={
        "sigmaif.libs": ["*.so"]
    }

)
