# SigmaIf Tensorflow

## Description
It's repo containing installable python package that uses `libsigmaif_op.so` built using cloned [sigmaif-building](https://gitlab.com/kacper1095/sigmaif-building) repository.

## Requirements
- miniconda 4.5.12 - [link](https://conda.io/en/latest/miniconda.html) to download and with installation instruction  (every version above it should be fine)
- CUDA 9.0 - [link](https://developer.nvidia.com/cuda-90-download-archive?) to download. Consider using `runfile(local)` only during download and don't forget to uncheck nvidia driver overriding during installation.
- CuDNN 7.1.4 - [link](https://developer.nvidia.com/rdp/cudnn-archive) to all versions. Download version appropriate for your operating system and for CUDA 9.0.
- nvidia driver 390.77
- cloned [`sigmaif-building`](https://gitlab.com/kacper1095/sigmaif-building/) and built package according to instruction from there

### Your current directory structure

```
<repos_directory>
  ├── sigmaif-building       <- required
  ├── sigmaif-tensorflow     <- currently in this directory
  └── sigmaif-testing        <- (if downloaded earlier, not required)
```

## Installation
Running in the top directory of this repository.
### Environment
1. `conda env create -f env.yaml`
2. `source activate sigmaif`

Now you have `sigmaif` environment activated which also includes dependencies used in [sigmaif-testing](https://gitlab.com/kacper1095/sigmaif-testing)  repository used for a researching.
### Package
Be sure to have `sigmaif` environment acivated using following command:
```
source activate sigmaif
```
Build a library according to [`sigmaif-building`](https://gitlab.com/kacper1095/sigmaif-building/) instruction and copy it manually:
```bash
cp ../sigmaif-building/projects/sigmaif/build/libsigma_if.so sigmaif/libs/
```
or run `update_library.sh` which runs the command above.


Install package using:
```
python setup.py install
```

### First check
Run the following command to check whether package has probably installed:
```bash
python
> from sigmaif import layer
> layer.SigmaIf
```
If no error is thrown, then your package is correctly installed.
Otherwise, be sure that you built your `libsigma_if.so` on your own and copied it this repo.

## Testing
### What is tested?:

1. Shape correctness of every possible variable.
2. Forward calculation and backward calculation on a dummy blob of data.
3. Both op and its usage in the `Layer` class.
4. Both CPU and GPU (but has to run separately in fact, maybe it will change in the future).
5. Grouping initialisation methods.
6. Sorting which is in fact performed in the op implementation.
7. Op convergence on dummy data.
8. Execution on relatively huge blob of data.

### Running
In the top directory of this repo:

- for GPU:
```bash
python -m pytest tests -p no:warnings -s
```

- for CPU:
```bash
CUDA_VISIBLE_DEVICES='' python -m pytest tests -p no:warnings -s
```


All tests should pass, both on CPU and GPU.



## Additional info
If you have `sigmaif-building` on the same directory level as this repo, you can use `update_library.sh` script to copy built library to this folder

## Remarks
This repo will be in the future merged with `sigmaif-building` so fewer steps will be needed to install appropriate dependencies.