import tensorflow as tf

from tensorflow.python.framework import ops
from tensorflow.python.keras import initializers
from tensorflow.python.ops.init_ops import Initializer

__all__ = ["Regular", "regular", "Exponential", "exponential", "Quadratic", "quadratic", "Fibonacci", "fibonacci"]


class Regular(Initializer):

    def __init__(self, group_size, seed=None):
        self.group_size = group_size
        self.seed = seed

    def __call__(self, shape, dtype=None, partition_info=None):
        features = shape[0]
        with ops.name_scope(None, "regular", [shape, self.group_size]) as name:
            groups = tf.cast(tf.range(features), dtype=tf.int32)
            groups = tf.div(groups, self.group_size)

            return groups

    def get_config(self):
        return {"group_size": self.group_size}


class Exponential(Initializer):
    def __init__(self, exponent):
        self.exponent = exponent

    def __call__(self, shape, dtype=None, partition_info=None):
        features = shape[0]
        with ops.name_scope(None, "regular", [shape]) as name:
            groups = tf.constant(self.generate_length_sequence(features), dtype=tf.int32)
            return groups

    def get_config(self):
        return {"exponent": self.exponent}

    def generate_length_sequence(self, length):
        result = []
        i = 0
        while len(result) < length:
            for _ in range(self.exponent ** i):
                result.append(i)
                if len(result) >= length:
                    return result
            i += 1
        return result


class Quadratic(Exponential):
    def __init__(self):
        super().__init__(2)


class Fibonacci(Initializer):

    def __call__(self, shape, dtype=None, partition_info=None):
        features = shape[0]
        with ops.name_scope(None, "regular", [shape]) as name:
            groups = tf.constant(self.generate_length_sequence(features), dtype=tf.int32)
            return groups

    def generate_length_sequence(self, length):
        result = []
        i = 0
        prev = 1
        prev_prev = 0
        while len(result) < length:
            for _ in range(prev):
                result.append(i)
                if len(result) >= length:
                    return result
            new = prev_prev + prev
            prev_prev = prev
            prev = new
            i += 1
        return result


def regular(group_size):
    return Regular(group_size)


def exponential(exponent):
    return Exponential(exponent)


def quadratic():
    return Quadratic()


def fibonacci():
    return Fibonacci()


for _method in __all__:
    setattr(initializers, _method, globals()[_method])
