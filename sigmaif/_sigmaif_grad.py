from pathlib import Path

import tensorflow as tf
from tensorflow.python.framework import ops

glob_path = Path(__file__).parent.absolute() / "libs" / "libsigma_if.so"
sigma_if_lib = tf.load_op_library(glob_path.as_posix())


@ops.RegisterGradient("SigmaIf")
def _sigma_if_grad_cc(op, *grads):
    layer_output_grad, _, _ = grads
    result_grads = sigma_if_lib.sigma_if_grad(layer_output_grad, op.inputs[0], op.inputs[1], op.inputs[4], op.inputs[6],
                                              op.outputs[1], op.outputs[2])
    grad_input, grad_weights = result_grads.grad_input, result_grads.grad_weights
    return [grad_input, grad_weights, None, None, None, None, None, None]
