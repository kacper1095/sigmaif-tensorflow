import tensorflow as tf
from tensorflow.python.framework import ops
from tensorflow.python.framework import tensor_shape
from tensorflow.python.keras import activations
from tensorflow.python.keras import constraints
from tensorflow.python.keras import initializers
from tensorflow.python.keras import layers as keras_layers
from tensorflow.python.keras import regularizers
from tensorflow.python.keras.engine import InputSpec
from tensorflow.python.layers import base

from sigmaif import sigma_if_lib
from sigmaif.group_classes_initializers import Regular


class _SigmaIfLayer(keras_layers.Layer):
    def __init__(self,
                 units,
                 threshold,
                 forward_fading_factor,
                 backward_fading_factor,
                 activation=None,
                 kernel_initializer='glorot_uniform',
                 kernel_regularizer=None,
                 activity_regularizer=None,
                 kernel_constraint=None,
                 group_initializer="regular",
                 **kwargs):

        if 'input_shape' not in kwargs and 'input_dim' in kwargs:
            kwargs['input_shape'] = (kwargs.pop('input_dim'),)

        super(_SigmaIfLayer, self).__init__(
            activity_regularizer=regularizers.get(activity_regularizer), **kwargs)
        self.units = int(units)
        self.threshold = float(threshold)
        self.forward_fading_factor = float(forward_fading_factor)
        self.backward_fading_factor = float(backward_fading_factor)

        self.activation = activations.get(activation)

        self.kernel_initializer = initializers.get(kernel_initializer)
        self.kernel_regularizer = regularizers.get(kernel_regularizer)
        self.kernel_constraint = constraints.get(kernel_constraint)

        self.group_initializer = initializers.get(group_initializer)

        self.supports_masking = True
        self.input_spec = [InputSpec(ndim=2), InputSpec(dtype=tf.bool)]

    def build(self, input_shape):
        input_shape, flag_shape = input_shape
        input_shape = tensor_shape.TensorShape(input_shape)
        if input_shape[-1].value is None:
            raise ValueError('The last dimension of the inputs to `SigmaIf` '
                             'should be defined. Found `None`.')
        self.input_spec = [InputSpec(ndim=2,
                                     axes={-1: input_shape[-1].value}),
                           InputSpec(dtype=tf.bool)]
        self.kernel = self.add_variable('kernel',
                                        shape=[input_shape[-1].value, self.units],
                                        initializer=self.kernel_initializer,
                                        regularizer=self.kernel_regularizer,
                                        constraint=self.kernel_constraint,
                                        dtype=self.dtype,
                                        trainable=True)

        self.groups = self.add_variable("groups",
                                        shape=[self.units, input_shape[-1].value],
                                        initializer=self._initialize_groups,
                                        regularizer=None,
                                        constraint=None,
                                        dtype=tf.int32,
                                        trainable=False)

        self.group_classes = self.add_variable("group_classes",
                                               shape=[input_shape[-1].value],
                                               initializer=self.group_initializer,
                                               regularizer=None,
                                               constraint=None,
                                               dtype=tf.int32,
                                               trainable=False)

        self.built = True

    def _initialize_groups(self, shape, dtype=None, partition_info=None):
        units, features = shape
        with ops.name_scope(None, "neurons_indices", [shape]) as name:
            groups = tf.cast(tf.tile(tf.reshape(tf.range(features), [1, -1]), [units, 1]), dtype=tf.int32)
            return groups

    def call(self, inputs, **kwargs):
        inputs, resort = inputs
        inputs = ops.convert_to_tensor(inputs, dtype=self.dtype)
        resort = ops.convert_to_tensor(resort, dtype=tf.bool)

        outputs = sigma_if_lib.sigma_if(inputs, self.kernel,
                                        self.threshold,
                                        self.forward_fading_factor,
                                        self.backward_fading_factor,
                                        self.groups,
                                        self.group_classes,
                                        resort)
        result = outputs.result
        used_neurons = outputs.used_neurons
        sorted_group_indices = outputs.sorted_group_indices

        assign_op = self.groups.assign(sorted_group_indices)

        with tf.control_dependencies([assign_op]):
            result += 0

        if self.activation is not None:
            return self.activation(result), used_neurons  # pylint: disable=not-callable
        return result, used_neurons

    def compute_output_shape(self, input_shape):
        input_shape = tensor_shape.TensorShape(input_shape)
        input_shape = input_shape.with_rank_at_least(2)
        if input_shape[-1].value is None:
            raise ValueError(
                'The innermost dimension of input_shape must be defined, but saw: %s'
                % input_shape)
        return input_shape[:-1].concatenate(self.units)

    def get_config(self):
        config = {
            'units': self.units,
            'threshold': self.threshold,
            'forward_fading_factor': self.forward_fading_factor,
            'backward_fading_factor': self.backward_fading_factor,
            'activation': activations.serialize(self.activation),
            'kernel_initializer': initializers.serialize(self.kernel_initializer),
            'kernel_regularizer': regularizers.serialize(self.kernel_regularizer),
            'activity_regularizer':
                regularizers.serialize(self.activity_regularizer),
            'kernel_constraint': constraints.serialize(self.kernel_constraint),
            'group_initializer': initializers.serialize(self.group_initializer)
        }
        base_config = super(_SigmaIfLayer, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


class SigmaIf(_SigmaIfLayer, base.Layer):

    def __init__(self,
                 units,
                 threshold,
                 activation=None,
                 kernel_initializer=None,
                 kernel_regularizer=None,
                 activity_regularizer=None,
                 kernel_constraint=None,
                 group_initializer=None,
                 trainable=True,
                 name=None,
                 **kwargs):
        super(SigmaIf, self).__init__(units=units,
                                      activation=activation,
                                      threshold=threshold,
                                      forward_fading_factor=1.,
                                      backward_fading_factor=1.,
                                      kernel_initializer=kernel_initializer,
                                      kernel_regularizer=kernel_regularizer,
                                      activity_regularizer=activity_regularizer,
                                      kernel_constraint=kernel_constraint,
                                      group_initializer=group_initializer,
                                      trainable=trainable,
                                      name=name,
                                      **kwargs)


def sigmaif(inputs,
            units,
            threshold,
            activation=None,
            kernel_initializer=None,
            kernel_regularizer=None,
            activity_regularizer=None,
            kernel_constraint=None,
            trainable=True,
            group_initializer=Regular(1),
            name=None,
            reuse=None):
    inputs, resort = inputs
    layer = SigmaIf(units,
                    threshold=threshold,
                    activation=activation,
                    kernel_initializer=kernel_initializer,
                    kernel_regularizer=kernel_regularizer,
                    activity_regularizer=activity_regularizer,
                    kernel_constraint=kernel_constraint,
                    group_initializer=group_initializer,
                    trainable=trainable,
                    name=name,
                    dtype=inputs.dtype.base_dtype,
                    _scope=name,
                    _reuse=reuse)
    return layer.apply([inputs, resort])


class FFASigmaIf(_SigmaIfLayer, base.Layer):

    def __init__(self,
                 units,
                 threshold,
                 forward_fading_factor,
                 backward_fading_factor,
                 activation=None,
                 kernel_initializer=None,
                 kernel_regularizer=None,
                 activity_regularizer=None,
                 kernel_constraint=None,
                 group_initializer=None,
                 trainable=True,
                 name=None,
                 **kwargs):
        super(FFASigmaIf, self).__init__(units=units,
                                         activation=activation,
                                         threshold=threshold,
                                         forward_fading_factor=forward_fading_factor,
                                         backward_fading_factor=backward_fading_factor,
                                         kernel_initializer=kernel_initializer,
                                         kernel_regularizer=kernel_regularizer,
                                         activity_regularizer=activity_regularizer,
                                         kernel_constraint=kernel_constraint,
                                         group_initializer=group_initializer,
                                         trainable=trainable,
                                         name=name,
                                         **kwargs)


def ffa_sigmaif(inputs,
                units,
                threshold,
                forward_fading_factor,
                backward_fading_factor,
                activation=None,
                kernel_initializer=None,
                kernel_regularizer=None,
                activity_regularizer=None,
                kernel_constraint=None,
                trainable=True,
                group_initializer=Regular(1),
                name=None,
                reuse=None):
    inputs, resort = inputs
    layer = FFASigmaIf(units,
                       threshold=threshold,
                       forward_fading_factor=forward_fading_factor,
                       backward_fading_factor=backward_fading_factor,
                       activation=activation,
                       kernel_initializer=kernel_initializer,
                       kernel_regularizer=kernel_regularizer,
                       activity_regularizer=activity_regularizer,
                       kernel_constraint=kernel_constraint,
                       group_initializer=group_initializer,
                       trainable=trainable,
                       name=name,
                       dtype=inputs.dtype.base_dtype,
                       _scope=name,
                       _reuse=reuse)
    return layer.apply([inputs, resort])
